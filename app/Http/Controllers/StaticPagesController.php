<?php

namespace App\Http\Controllers;

class StaticPagesController extends Controller
{
    /**
     * @return mixed
     */
    public function home()
    {
        return view('static_pages/home');
    }

    /**
     *
     * @return mixed
     */
    public function about()
    {
        return view('static_pages/about');
    }

    /**
     * @return mixed
     */
    public function help()
    {
        return view('static_pages/help');
    }

    /**
     * @return mixed
     */
    public function contact()
    {
        return view('static_pages/contact');
    }
}
