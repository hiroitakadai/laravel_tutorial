<!DOCTYPE html>
<html>
<head>
    <title>@yield('title') | Ruby on Rails Tutorial Sample App</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{ Html::script('js/app.js', ["media" => "all", "data-turbolinks-track" => "reload"]) }}
    {{ Html::style('css/app.css', ["media" => "all", "data-turbolinks-track" => "reload"]) }}
</head>

<body>
@yield('content')
</body>
</html>
