<?php

namespace Tests\Unit;

use Tests\TestCase;

class StaticPagesControllerTest extends TestCase
{
    private $base_title;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->base_title = "Ruby on Rails Tutorial Sample App";
    }

    /**
     *
     */
    public function testGetRoot()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
        $dom = $this->dom($response->content());
        $this->assertSame("Home | {$this->base_title}", $dom->filter("title")->text());
    }

    /**
     *
     */
    public function testGetHome()
    {
        $response = $this->get('/static_pages/home');
        $response->assertStatus(200);
        $dom = $this->dom($response->content());
        $this->assertSame("Home | {$this->base_title}", $dom->filter("title")->text());
    }

    /**
     *
     */
    public function testGetAbout()
    {
        $response = $this->get('/static_pages/about');
        $response->assertStatus(200);
        $dom = $this->dom($response->content());
        $this->assertSame("About | {$this->base_title}", $dom->filter("title")->text());
    }

    /**
     *
     */
    public function testGetHelp()
    {
        $response = $this->get('/static_pages/help');
        $response->assertStatus(200);
        $dom = $this->dom($response->content());
        $this->assertSame("Help | {$this->base_title}", $dom->filter("title")->text());
    }

    /**
     *
     */
    public function testGetContact()
    {
        $response = $this->get('/static_pages/contact');
        $response->assertStatus(200);
        $dom = $this->dom($response->content());
        $this->assertSame("Contact | {$this->base_title}", $dom->filter("title")->text());
    }
}
