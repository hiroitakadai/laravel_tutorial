<?php

use App\Http\Controllers\StaticPagesController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [StaticPagesController::class, 'home']);
Route::get('static_pages/home', [StaticPagesController::class, 'home']);
Route::get('static_pages/about', [StaticPagesController::class, 'about']);
Route::get('static_pages/help', [StaticPagesController::class, 'help']);
Route::get('static_pages/contact', [StaticPagesController::class, 'contact']);
